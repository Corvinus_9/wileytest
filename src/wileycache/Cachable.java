/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wileycache;

/**
 *
 * @author Arseny
 */
public interface Cachable <K, V> {
    void cache (K key, V value) throws Exception;
    void clearCache();
    V getObject(K key) throws Exception;
    boolean containsKey(K key);
    int size();
    
}
