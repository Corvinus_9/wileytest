
import java.util.LinkedHashMap;
import java.util.Map;
import wileycache.Cachable;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Arseny
 */
class MemoryCache<K, V> implements Cachable <K, V> {

    
    int MAX_CAPACITY;
    Map <K, V> lruCache = null; 
        
    public MemoryCache (int capacity){
         MAX_CAPACITY = capacity;
         lruCache = new LinkedHashMap <K, V>(MAX_CAPACITY, 0.75f, true){
            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                return size() > MAX_CAPACITY;
            }
        };
    }

    @Override
    public void cache(K key, V value) throws Exception {
        lruCache.put(key, value);
    }

    @Override
    public void clearCache() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public V getObject(K key) throws Exception {
        
        if (lruCache.containsKey(key))
             return lruCache.get(key);
        return null;
    }

    @Override
    public boolean containsKey(K key) {
        return lruCache.containsKey(key);
    }

    @Override
    public int size() {
        return lruCache.size();
    }
    
   
    
}
