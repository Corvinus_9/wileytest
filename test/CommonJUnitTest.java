/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import wileycache.Cachable;

/**
 *
 * @author Arseny
 */
public class CommonJUnitTest {
    
    public CommonJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Test
    public void CacheMemoryTest() {
        try {
            Cachable cache = new MemoryCache(1000);
            Object obj = new String("Test1");
            Object key = new String("1");
            cache.cache(key, obj);
            assertTrue(obj.equals(cache.getObject(key)));
        } catch (Exception ex) {
            Logger.getLogger(CommonJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void CacheMemoryTestCapacity() {
        try {
            Cachable cache = new MemoryCache(1);
            Object obj = new String("Test1");
            Object key = new String("1");
            Object obj2 = new String("Test2");
            Object key2 = new String("2");
            cache.cache(key, obj);
            assertTrue(obj.equals(cache.getObject(key)));
            cache.cache(key2, obj2);
            assertFalse(cache.containsKey(key));
        } catch (Exception ex) {
            Logger.getLogger(CommonJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}